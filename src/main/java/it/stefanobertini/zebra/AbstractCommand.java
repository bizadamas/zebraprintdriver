package it.stefanobertini.zebra;

public abstract class AbstractCommand {
    // public abstract class AbstractCommand implements CommandInterface {

    private StringBuffer commandText = new StringBuffer();

    // public StringBuffer getCommandTextBuffer() {
    // return commandText;
    // }

    protected void appendText(String text) {
        commandText.append(text == null ? "" : text);
    }

    protected void appendText(int text) {
        commandText.append("" + text);
    }

    protected void appendText(double text) {
        commandText.append(FormatUtils.format(text));
    }

    protected void endLine() {
        commandText.append("\r\n");
    }

    public abstract void validate();

    protected abstract String getCommand();

    protected abstract void getCommandLineInternal();

    /**
     * This default implementation just returns the command line byte array.<br/>
     * Some special commands (@see it.bertasoft.zebra.cpcl.Pcx ) can override
     * this function to implement some specific behaviour
     * 
     * @see it.stefanobertini.zebra.CommandInterface#getCommandByteArray()
     */
    public byte[] getCommandByteArray() {

        validate();

        commandText = new StringBuffer();

        getCommandLineInternal();

        return commandText.toString().getBytes();
    }

}
