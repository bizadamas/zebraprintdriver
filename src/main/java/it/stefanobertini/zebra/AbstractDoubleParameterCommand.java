package it.stefanobertini.zebra;

public abstract class AbstractDoubleParameterCommand extends AbstractCommand {

    private double parameter;

    public void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(parameter);
        endLine();
    }

    public double getParameter() {
        return parameter;
    }

    public void setParameter(double parameter) {
        this.parameter = parameter;
    }

}
