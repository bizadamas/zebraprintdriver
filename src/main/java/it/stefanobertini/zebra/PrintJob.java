package it.stefanobertini.zebra;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PrintJob<T extends CommandInterface> {

    private List<T> commands = new ArrayList<T>();

    public PrintJob<T> add(T command) {
        commands.add(command);
        return this;
    }

    // public String getCommandLine() {
    // StringBuffer buffer = new StringBuffer();
    // for (CommandInterface command : commands) {
    // buffer.append(command.getCommandLine());
    // }
    // return buffer.toString();
    // }

    public byte[] getCommandByteArray() {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        for (CommandInterface command : commands) {
            try {
                buffer.write(command.getCommandByteArray());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return buffer.toByteArray();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PrintJob [commands=" + commands + "]";
    }

}
