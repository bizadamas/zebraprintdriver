package it.stefanobertini.zebra;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class NetworkPrintDriver {
    private String address;
    private int port;

    private Socket socket = null;
    private DataOutputStream outputStream = null;
    private DataInputStream inputStream;
    private InputStreamReader inputStreamReader;
    private PrintStream debugStream;

    private long defaultAnswerWaitMillis = 1000;

    private final static int BUFFER_SIZE = 2048;

    public NetworkPrintDriver(String address, int port) {
        super();
        this.address = address;
        this.port = port;
    }

    public void setDefaultAnswerWaitMillis(long defaultAnswerWaitMillis) {
        this.defaultAnswerWaitMillis = defaultAnswerWaitMillis;
    }

    public void setDebugStream(PrintStream debugStream) {
        this.debugStream = debugStream;
    }

    public void open() throws UnknownHostException, IOException {
        socket = new Socket(address, port);

        outputStream = new DataOutputStream(socket.getOutputStream());
        inputStream = new DataInputStream(socket.getInputStream());
        inputStreamReader = new InputStreamReader(socket.getInputStream());
    }

    public void close() {
        closeQuietly(outputStream);
        closeQuietly(inputStreamReader);

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // Don't care
            }
        }
    }

    public String executeNoWait(CommandInterface command) throws Exception {
        return executeAndWait(command, 0);
    }

    public String executeAndWait(CommandInterface command) throws Exception {
        return executeAndWait(command, defaultAnswerWaitMillis);
    }

    public String executeAndWait(CommandInterface command, long answerWaitMillis) throws Exception {
        byte[] commandBytes;

        clearBuffer();

        commandBytes = command.getCommandByteArray();

        if (debugStream != null) {
            debugStream.print("Driver: ");
            debugStream.println(new String(commandBytes));
        }
        outputStream.write(commandBytes);

        return readAnswer(answerWaitMillis);
    }

    @SuppressWarnings("rawtypes")
    public void execute(PrintJob commands) throws Exception {
        byte[] commandBytes;

        clearBuffer();

        commandBytes = commands.getCommandByteArray();

        if (debugStream != null) {
            debugStream.print("Driver: ");
            debugStream.println(new String(commandBytes));
        }

        outputStream.write(commandBytes);
    }

    public void clearBuffer() throws IOException {
        readAnswer();
    }

    public void waitForAnswer(long answerWaitMillis) throws IOException {
        long start;
        start = System.currentTimeMillis();

        while (inputStream.available() == 0 && System.currentTimeMillis() < start + answerWaitMillis) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // Don't care
            }
        }
    }

    public String readAnswer() throws IOException {

        StringBuffer buffer = new StringBuffer();

        while (inputStream.available() > 0) {
            byte[] byteBuffer = new byte[BUFFER_SIZE];
            int bytesRead = inputStream.read(byteBuffer, 0, BUFFER_SIZE);

            buffer.append(new String(byteBuffer, 0, bytesRead));
        }

        if (buffer.length() > 2) {
            if (buffer.charAt(0) == '"') {
                buffer.delete(0, 1);
            }
            if (buffer.charAt(buffer.length() - 1) == '"') {
                buffer.delete(buffer.length() - 1, buffer.length());
            }
        }
        return buffer.toString();
    }

    public String readAnswer(long millis) throws IOException {
        waitForAnswer(millis);
        return readAnswer();
    }

    private void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException ioe) {
            // Don't care
        }
    }

}
