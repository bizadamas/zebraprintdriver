package it.stefanobertini.zebra;

public abstract class AbstractStringParameterCommand extends AbstractCommand {

    private String parameter;

    @Override
    public void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(parameter);
        endLine();
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

}
