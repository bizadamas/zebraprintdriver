package it.stefanobertini.zebra;

public abstract class AbstractNoParameterCommand extends AbstractCommand {

    @Override
    public void getCommandLineInternal() {
        appendText(getCommand());
        endLine();
    }

}
