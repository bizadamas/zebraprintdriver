package it.stefanobertini.zebra.enums;

public enum DefineFileTerminator {

    print("PRINT"), end("END");

    private final String code;

    private DefineFileTerminator(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
