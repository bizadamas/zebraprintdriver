package it.stefanobertini.zebra.enums;

public enum PaperJamMethod {

    presentation("PRESENTATION"), bar("BAR"), gap("GAP");

    private final String code;

    private PaperJamMethod(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
