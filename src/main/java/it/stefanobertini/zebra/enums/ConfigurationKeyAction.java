package it.stefanobertini.zebra.enums;

public enum ConfigurationKeyAction {
    SETVAR, GETVAR, DO;
}
