package it.stefanobertini.zebra.enums;

public enum OutOfPaperMode {

    purge("PURGE"), wait("WAIT");

    private final String code;

    private OutOfPaperMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
