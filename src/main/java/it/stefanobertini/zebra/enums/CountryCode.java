package it.stefanobertini.zebra.enums;

public enum CountryCode {

    usa("USA"), sweden("SWEDEN"), italy("ITALY"), latin9("LATIN9"), korea("KOREA"), germany("GERMANY"), spain("SPAIN"), cp850("CP850"), cp874("CP874"), big5(
            "BIG5"), france("FRANCE"), norway("NORWAY"), unitedKingdom("UK"), china("CHINA"), japan_s("JAPAN-S");

    private final String code;

    private CountryCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
