package it.stefanobertini.zebra.enums;

public enum JustificationType {

    center("CENTER"), left("LEFT"), right("RIGHT");

    private final String code;

    private JustificationType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
