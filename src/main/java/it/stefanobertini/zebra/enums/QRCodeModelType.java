package it.stefanobertini.zebra.enums;

public enum QRCodeModelType {

    model1("1"), model2("2");

    private final String code;

    private QRCodeModelType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
