package it.stefanobertini.zebra.enums;

public enum BarcodeType {

    UPCA("UPCA"), UPCA2("UPCA2"), UPCA5("UPCA5"), UPCE("UPCE"), UPCE2("UPCE2"), UPCE5("UPCE5"), EAN13("EAN13"), EAN132("EAN132"), EAN135("EAN135"), Code39("39"), Code39C(
            "39C"), Code39F("F39"), Code39FC("F39C"), Interleaved2of5("I2OF5"), Interleaved2of5withChecksum("I2OF5C"), GermanPostCode("I2OF5G"), Code128("128"), UccEan128(
            "UCCEAN128"), MSIPlessy("MSI"), MSIPlessy10("MSI10"), MSIPlessy1010("MSI1010"), MSIPlessyI1110("MSI1110"), Postnet("POSTNET"), FIM("FIM");

    private final String code;

    private BarcodeType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
