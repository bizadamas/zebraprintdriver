package it.stefanobertini.zebra.enums;

public enum QRCodeCharacterMode {

    numeric("N"), alphanumeric("A"), binary("B"), kanji("K");

    private final String code;

    private QRCodeCharacterMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
