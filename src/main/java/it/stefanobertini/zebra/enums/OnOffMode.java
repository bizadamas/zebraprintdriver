package it.stefanobertini.zebra.enums;

public enum OnOffMode {
    on, off;
}
