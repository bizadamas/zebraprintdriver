package it.stefanobertini.zebra.enums;

public enum OnFeedMode {

    ignore("IGNORE"), reprint("REPRINT"), feed("FEED");

    private final String code;

    private OnFeedMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
