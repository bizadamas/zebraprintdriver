package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.enums.QRCodeCharacterMode;
import it.stefanobertini.zebra.enums.QRCodeErrorCorrectionLevel;
import it.stefanobertini.zebra.enums.QRCodeMaskNumber;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class QRCodeManualData extends QRCodeAbstractData implements QRCodeDataInterface {

    private List<QRCodeManualDataItem> items;

    public QRCodeManualData(QRCodeErrorCorrectionLevel errorCorrectionLevel, QRCodeMaskNumber maskNumber) {
        this(errorCorrectionLevel, maskNumber, new ArrayList<QRCodeManualDataItem>());
    }

    public QRCodeManualData(QRCodeErrorCorrectionLevel errorCorrectionLevel, QRCodeMaskNumber maskNumber, List<QRCodeManualDataItem> items) {
        super(errorCorrectionLevel, maskNumber, "M");
        this.items = items;
    }

    public void addData(QRCodeManualDataItem item) {
        items.add(item);
    }

    public String getCommandLine() {
        StringBuffer buffer = new StringBuffer();
        DecimalFormat decimalFormat = new DecimalFormat("0000");

        buffer.append(super.getCommandLine());
        for (int i = 0; i < items.size(); i++) {
            QRCodeManualDataItem item = items.get(i);
            if (i > 0) {
                buffer.append(",");
            }
            buffer.append(item.getCharacterMode().getCode());

            if (QRCodeCharacterMode.binary.equals(item.getCharacterMode())) {
                buffer.append(decimalFormat.format(item.getData().length()));
            }

            buffer.append(item.getData());
        }
        buffer.append("\r\n");

        return buffer.toString();
    }

}
