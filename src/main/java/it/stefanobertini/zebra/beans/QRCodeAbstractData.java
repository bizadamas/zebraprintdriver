package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.enums.QRCodeErrorCorrectionLevel;
import it.stefanobertini.zebra.enums.QRCodeMaskNumber;

public abstract class QRCodeAbstractData implements QRCodeDataInterface {
    protected QRCodeErrorCorrectionLevel errorCorrectionLevel;
    protected QRCodeMaskNumber maskNumber = QRCodeMaskNumber.none;
    protected String dataInputMode;

    public QRCodeAbstractData(QRCodeErrorCorrectionLevel errorCorrectionLevel, QRCodeMaskNumber maskNumber, String dataInputMode) {
        super();
        this.errorCorrectionLevel = errorCorrectionLevel;
        this.maskNumber = maskNumber;
        this.dataInputMode = dataInputMode;
    }

    public String getCommandLine() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(errorCorrectionLevel.getCode());
        buffer.append(maskNumber.getCode());
        buffer.append(dataInputMode);
        buffer.append(",");

        return buffer.toString();
    }
}