package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.FormatUtils;

public class Size {

    private double width = 0;
    private double height = 0;

    public Size(double width, double height) {
        super();
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getCommandLine() {
        return FormatUtils.format(width) + " " + FormatUtils.format(height);
    }

    @Override
    public String toString() {
        return "Size [width=" + FormatUtils.format(width) + ", height=" + FormatUtils.format(height) + "]";
    }

}