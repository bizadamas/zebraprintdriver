package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class OnLowBattery extends AbstractCommand implements CommandInterface {

    private String alertMessage;
    private int alarmDuration;

    public OnLowBattery() {
        this("", 0);
    }

    public OnLowBattery(String alertMessage) {
        this(alertMessage, 0);
    }

    public OnLowBattery(int alarmDuration) {
        this("", alarmDuration);
    }

    public OnLowBattery(String alertMessage, int alarmDuration) {
        super();
        this.alertMessage = alertMessage;
        this.alarmDuration = alarmDuration;
    }

    @Override
    public String getCommand() {
        return "OLB";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());

        if (alertMessage != null && alertMessage.length() > 0) {
            appendText(" ALERT \"");
            appendText(alertMessage);
            appendText("\"");
        }
        if (alarmDuration > 0) {
            appendText(" ALARM ");
            appendText(alarmDuration);
        }
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        if ((alertMessage == null || alertMessage.length() == 0) && alarmDuration <= 0) {
            Validator.signalError("Specify at least one parameter between 'alertMessage' and 'alarmDuration'");
        }
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public int getAlarmDuration() {
        return alarmDuration;
    }

    public void setAlarmDuration(int alarmDuration) {
        this.alarmDuration = alarmDuration;
    }

}
