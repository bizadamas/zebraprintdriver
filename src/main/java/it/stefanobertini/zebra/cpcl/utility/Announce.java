package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class Announce extends AbstractCommand implements CommandInterface {

    private String message;

    public Announce() {
        this("");
    }

    public Announce(String message) {
        super();
        this.message = message;
    }

    @Override
    public String getCommand() {
        return "ANNOUNCE";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(message);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("message", message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Announce [message=" + message + "]";
    }

}
