package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.LineTerminatorCharacter;

public class SetLineTerminatorCharacter extends AbstractCommand implements CommandInterface {

    private LineTerminatorCharacter lineTerminatorCharacter;

    public SetLineTerminatorCharacter() {
        this(LineTerminatorCharacter.carriageReturnAndLineFeed);
    }

    public SetLineTerminatorCharacter(LineTerminatorCharacter lineTerminatorCharacter) {
        super();
        this.lineTerminatorCharacter = lineTerminatorCharacter;
    }

    @Override
    public String getCommand() {
        return "LT";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(lineTerminatorCharacter.getCode());
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("lineTerminatorCharacter", lineTerminatorCharacter);
    }

    public LineTerminatorCharacter getLineTerminatorCharacter() {
        return lineTerminatorCharacter;
    }

    public void setLineTerminatorCharacter(LineTerminatorCharacter lineTerminatorCharacter) {
        this.lineTerminatorCharacter = lineTerminatorCharacter;
    }

    @Override
    public String toString() {
        return "SetLineTerminatorCharacter [lineTerminatorCharacter=" + lineTerminatorCharacter + "]";
    }

}
