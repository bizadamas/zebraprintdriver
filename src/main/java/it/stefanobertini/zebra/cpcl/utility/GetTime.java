package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;

public class GetTime extends AbstractCommand implements CommandInterface {

    @Override
    public String getCommand() {
        return "GET-TIME";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "GetTime []";
    }
}
