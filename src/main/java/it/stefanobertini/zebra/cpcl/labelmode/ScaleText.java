package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

public class ScaleText extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation = Orientation.horizontal;
    private String name;
    private int fontWidth;
    private int fontHeight;
    private Position position = new Position(0, 0);
    private String text = "";

    public ScaleText(String name, int fontWidth, int fontHeight) {
        this(Orientation.horizontal, name, fontWidth, fontHeight, new Position(0, 0), "");
    }

    public ScaleText(Orientation orientation, String name, int fontWidth, int fontHeight, Position position, String text) {
        super();
        this.orientation = orientation;
        this.name = name;
        this.fontWidth = fontWidth;
        this.fontHeight = fontHeight;
        this.position = position;
        this.text = text;
    }

    public String getCommand() {
        return Orientation.vertical.equals(orientation) ? "VSCALE-TEXT" : "SCALE-TEXT";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(name);
        appendText(" ");
        appendText(fontWidth);
        appendText(" ");
        appendText(fontHeight);
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(text);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isRequired("name", name);
        Validator.isRequired("text", text);
        Validator.isMoreThanOrEqualTo("fontWidth", fontWidth, 0);
        Validator.isMoreThanOrEqualTo("fontHeight", fontHeight, 0);
    }

    /**
     * @return the orientation
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the fontWidth
     */
    public int getFontWidth() {
        return fontWidth;
    }

    /**
     * @param fontWidth
     *            the fontWidth to set
     */
    public void setFontWidth(int fontWidth) {
        this.fontWidth = fontWidth;
    }

    /**
     * @return the fontHeight
     */
    public int getFontHeight() {
        return fontHeight;
    }

    /**
     * @param fontHeight
     *            the fontHeight to set
     */
    public void setFontHeight(int fontHeight) {
        this.fontHeight = fontHeight;
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScaleText [orientation=" + orientation + ", name=" + name + ", fontWidth=" + fontWidth + ", fontHeight=" + fontHeight + ", position="
                + position + ", text=" + text + "]";
    }

}
