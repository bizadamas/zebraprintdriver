package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetSpacing extends AbstractDoubleParameterCommand implements LabelModeCommandInterface {

    public SetSpacing() {
        this(0);
    }

    public SetSpacing(double spacing) {
        setParameter(spacing);
    }

    public String getCommand() {
        return "SETSP";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("spacing", getParameter(), 0);
    }

    public double getSpacing() {
        return getParameter();
    }

    public void setSpacing(double spacing) {
        setParameter(spacing);
    }

    @Override
    public String toString() {
        return "SetSpacing [spacing=" + getParameter() + "]";
    }

}
