package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class Cut extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "CUT";
    }

    @Override
    public void validate() {
    }
}
