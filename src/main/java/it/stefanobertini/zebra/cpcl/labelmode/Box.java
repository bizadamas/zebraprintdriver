package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;

public class Box extends AbstractCommand implements LabelModeCommandInterface {

    private Position topLeft;
    private Position bottomRight;
    private double width;

    public Box(Position topLeft, Position bottomRight) {
        this(topLeft, bottomRight, 1);
    }

    public Box(Position topLeft, Position bottomRight, double width) {
        super();
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.width = width;
    }

    public String getCommand() {
        return "BOX";
    }

    @Override
    public void validate() {
        Validator.isRequired("topLeft", topLeft);
        Validator.isRequired("bottomRight", bottomRight);
        Validator.isMoreThanOrEqualTo("width", width, 0);
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(topLeft.getCommandLine());
        appendText(" ");
        appendText(bottomRight.getCommandLine());
        appendText(" ");
        appendText(width);
        endLine();
    }

    public Position getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Position topLeft) {
        this.topLeft = topLeft;
    }

    public Position getBottomRight() {
        return bottomRight;
    }

    public void setBottomRight(Position bottomRight) {
        this.bottomRight = bottomRight;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Box [topLeft=" + topLeft + ", bottomRight=" + bottomRight + ", width=" + width + "]";
    }

}
