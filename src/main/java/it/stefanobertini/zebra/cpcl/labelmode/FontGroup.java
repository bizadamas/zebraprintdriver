package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;

import java.util.ArrayList;
import java.util.List;

public class FontGroup extends AbstractCommand implements LabelModeCommandInterface {

    private int groupNumber;
    private List<Font> fonts = new ArrayList<Font>();

    public FontGroup(int groupNumber) {
        super();
        this.groupNumber = groupNumber;
    }

    public FontGroup(int groupNumber, List<Font> fonts) {
        super();
        this.groupNumber = groupNumber;
        this.fonts = fonts;
    }

    public FontGroup addFont(Font font) {
        fonts.add(font);
        return this;
    }

    public FontGroup addFont(String font, int size) {
        addFont(new Font(font, size));
        return this;
    }

    public String getCommand() {
        return "FG";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("FG");
        appendText(" ");
        appendText(groupNumber);

        for (Font font : fonts) {
            appendText(" ");
            appendText(font.getCommandLine());
        }
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("fonts", fonts);
        Validator.isBetween("groupNumber", groupNumber, 0, 9);
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public List<Font> getFonts() {
        return fonts;
    }

    public void setFonts(List<Font> fonts) {
        this.fonts = fonts;
    }

    @Override
    public String toString() {
        return "FontGroup [groupNumber=" + groupNumber + ", fonts=" + fonts + "]";
    }

}
