package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.OnFeedMode;

public class OnFeed extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public OnFeed() {
        this(OnFeedMode.feed);
    }

    public OnFeed(String onFeedMode) {
        setParameter(onFeedMode);
    }

    public OnFeed(OnFeedMode onFeedMode) {
        this.setParameter(onFeedMode.getCode());
    }

    public String getCommand() {
        return "ON-FEED";
    }

    @Override
    public void validate() {
        Validator.isRequired("onFeedMode", getParameter());
    }

    public String getOnFeedMode() {
        return getParameter();
    }

    public void setOnFeedMode(String onFeedMode) {
        setParameter(onFeedMode);
    }

    public void setOnFeedMode(OnFeedMode onFeedMode) {
        setParameter(onFeedMode.getCode());
    }

    @Override
    public String toString() {
        return "OnFeed [onFeedMode=" + getParameter() + "]";
    }

}
