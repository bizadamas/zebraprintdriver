package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class PresentAt extends AbstractCommand implements LabelModeCommandInterface {

    private double length;
    private int delay;

    public PresentAt() {
        this(0, 0);
    }

    public PresentAt(double length, int delay) {
        super();
        this.length = length;
        this.delay = delay;
    }

    public String getCommand() {
        return "PRESENT-AT";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(length);
        appendText(" ");
        appendText(delay);
        endLine();

    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("length", length, 0);
        Validator.isMoreThanOrEqualTo("delay", delay, 0);
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the delay
     */
    public int getDelay() {
        return delay;
    }

    /**
     * @param delay
     *            the delay to set
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PresentAt [length=" + length + ", delay=" + delay + "]";
    }

}
