package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.ContrastLevel;

public class Contrast extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Contrast() {
        this(0);
    }

    public Contrast(int contrastLevel) {
        setParameter(contrastLevel);
    }

    public Contrast(ContrastLevel contrastLevel) {
        setParameter(contrastLevel.getCode());
    }

    public String getCommand() {
        return "CONTRAST";
    }

    @Override
    public void validate() {
        Validator.isBetween("contrast", getParameter(), 0, 3);
    }

    public int getContrast() {
        return getParameter();
    }

    public void setContrast(int contrastLevel) {
        setParameter(contrastLevel);
    }

    public void setContrast(ContrastLevel contrastLevel) {
        setParameter(contrastLevel.getCode());
    }

    @Override
    public String toString() {
        return "Contrast [contrastLevel=" + getParameter() + "]";
    }

}
