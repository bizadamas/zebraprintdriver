package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.Orientation;

public class Barcode extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation = Orientation.horizontal;
    private String barcodeType;
    private double width;
    private String ratio;
    private double height;
    private Position position;
    private Font font = null;
    private String data;
    private double offset;

    public Barcode(BarcodeType barcodeType, BarcodeRatio ratio, double width, double height, Position position, String data) {
        this(Orientation.horizontal, barcodeType, ratio, width, height, position, data);
    }

    public Barcode(String barcodeType, String ratio, double width, double height, Position position, String data) {
        this(Orientation.horizontal, barcodeType, ratio, width, height, position, data);
    }

    public Barcode(BarcodeType barcodeType, BarcodeRatio ratio, double width, double height, Position position, String data, Font font, double offset) {
        this(Orientation.horizontal, barcodeType, ratio, width, height, position, data, font, offset);
    }

    public Barcode(String barcodeType, String ratio, double width, double height, Position position, String data, Font font, double offset) {
        this(Orientation.horizontal, barcodeType, ratio, width, height, position, data, font, offset);
    }

    public Barcode(Orientation orientation, BarcodeType barcodeType, BarcodeRatio ratio, double width, double height, Position position, String data) {
        this(orientation, barcodeType, ratio, width, height, position, data, null, 0);
    }

    public Barcode(Orientation orientation, BarcodeType barcodeType, String ratio, double width, double height, Position position, String data) {
        this(orientation, barcodeType, ratio, width, height, position, data, null, 0);
    }

    public Barcode(Orientation orientation, String barcodeType, String ratio, double width, double height, Position position, String data) {
        this(orientation, barcodeType, ratio, width, height, position, data, null, 0);
    }

    public Barcode(Orientation orientation, BarcodeType barcodeType, BarcodeRatio ratio, double width, double height, Position position, String data,
            Font font, double offset) {
        this(orientation, barcodeType.getCode(), ratio.getCode(), width, height, position, data, font, offset);
    }

    public Barcode(Orientation orientation, BarcodeType barcodeType, String ratio, double width, double height, Position position, String data, Font font,
            double offset) {
        this(orientation, barcodeType.getCode(), ratio, width, height, position, data, font, offset);
    }

    public Barcode(Orientation orientation, String barcodeType, String ratio, double width, double height, Position position, String data, Font font,
            double offset) {
        super();
        this.orientation = orientation;
        this.barcodeType = barcodeType;
        this.width = width;
        this.ratio = ratio;
        this.height = height;
        this.position = position;
        this.data = data;
        this.font = font;
        this.offset = offset;
    }

    public String getCommand() {
        return Orientation.vertical.equals(orientation) ? "VBARCODE" : "BARCODE";
    }

    @Override
    public void getCommandLineInternal() {

        // Se specifico un font, stampo anche il testo sottostante
        if (font != null) {
            BarcodeText barcodeText = new BarcodeText(font, offset);
            appendText(new String(barcodeText.getCommandByteArray()));
        }

        appendText(getCommand());

        appendText(" ");
        appendText(barcodeType);
        appendText(" ");
        appendText(width);
        appendText(" ");
        appendText(ratio);
        appendText(" ");
        appendText(height);
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(data);

        endLine();

        if (font != null) {
            BarcodeTextOff barcodeTextOff = new BarcodeTextOff();
            appendText(new String(barcodeTextOff.getCommandByteArray()));
        }
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isRequired("barcodeType", barcodeType);
        Validator.isRequired("ratio", ratio);
        Validator.isRequired("data", data);
        Validator.isMoreThanOrEqualTo("width", width, 0);
        Validator.isMoreThanOrEqualTo("height", height, 0);
    }

    /**
     * @return the orientation
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the barcodeType
     */
    public String getBarcodeType() {
        return barcodeType;
    }

    /**
     * @param barcodeType
     *            the barcodeType to set
     */
    public void setBarcodeType(String barcodeType) {
        this.barcodeType = barcodeType;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the ratio
     */
    public String getRatio() {
        return ratio;
    }

    /**
     * @param ratio
     *            the ratio to set
     */
    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the font
     */
    public Font getFont() {
        return font;
    }

    /**
     * @param font
     *            the font to set
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the offset
     */
    public double getOffset() {
        return offset;
    }

    /**
     * @param offset
     *            the offset to set
     */
    public void setOffset(double offset) {
        this.offset = offset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Barcode [orientation=" + orientation + ", barcodeType=" + barcodeType + ", width=" + width + ", ratio=" + ratio + ", height=" + height
                + ", position=" + position + ", font=" + font + ", data=" + data + ", offset=" + offset + "]";
    }

}
