package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.PatternType;

/**
 * 
 * Implements the ENCODING command<br/>
 * <br/>
 * Description:<br>
 * The ENCODING command specifies the encoding of data sent to the printer. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * ENCODING {encodingType}
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Pattern extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public Pattern() {
    }

    public Pattern(String patternType) {
        setParameter(patternType);
    }

    public Pattern(PatternType patternType) {
        this.setParameter(patternType.getCode());
    }

    public String getCommand() {
        return "PATTERN";
    }

    @Override
    public void validate() {
        Validator.isRequired("patternType", getParameter());
    }

    public String getPatternType() {
        return getParameter();
    }

    public void setPatternType(String patternType) {
        setParameter(patternType);
    }

    public void setEncodingType(PatternType patternType) {
        setParameter(patternType.getCode());
    }

    @Override
    public String toString() {
        return "Pattern [patternType=" + getParameter() + "]";
    }

}
