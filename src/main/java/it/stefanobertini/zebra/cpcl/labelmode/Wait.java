package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class Wait extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Wait() {
        this(0);
    }

    public Wait(int delayTime) {
        setParameter(delayTime);
    }

    public String getCommand() {
        return "WAIT";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("delayTime", getParameter(), 0);
    }

    public int getDelayTime() {
        return getParameter();
    }

    public void setDelayTime(int delayTime) {
        setParameter(delayTime);
    }

    @Override
    public String toString() {
        return "Wait [delayTime=" + getParameter() + "]";
    }

}
