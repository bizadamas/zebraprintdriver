package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

public class Aztec extends AbstractCommand implements LabelModeCommandInterface {
    private Orientation orientation = Orientation.horizontal;
    private Position position;
    private double unitWidth = 6;
    private int errorCorrection = 0;

    private String data = "";

    public Aztec() {
    }

    public Aztec(Position position, String data) {
        this(Orientation.horizontal, position, 6, 0, data);
    }

    public Aztec(Orientation orientation, Position position, double unitWidth, int errorCorrection, String data) {
        super();
        this.orientation = orientation;
        this.position = position;
        this.unitWidth = unitWidth;
        this.errorCorrection = errorCorrection;
        this.data = data;
    }

    public String getCommand() {
        return (Orientation.vertical.equals(orientation) ? "VBARCODE" : "BARCODE") + " AZTEC";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText("XD");
        appendText(" ");
        appendText(unitWidth);
        appendText(" ");
        appendText("EC");
        appendText(" ");
        appendText(errorCorrection);
        endLine();

        appendText(data);
        endLine();

        appendText("ENDAZTEC");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("position", position);
        Validator.isRequired("orientation", orientation);
        Validator.isMoreThanOrEqualTo("unitWidth", unitWidth, 0);
        Validator.isBetween("errorCorrection", errorCorrection, 0, 99);
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public double getUnitWidth() {
        return unitWidth;
    }

    public void setUnitWidth(double unitWidth) {
        this.unitWidth = unitWidth;
    }

    public int getErrorCorrection() {
        return errorCorrection;
    }

    public void setErrorCorrection(int errorCorrection) {
        this.errorCorrection = errorCorrection;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Aztec [orientation=" + orientation + ", position=" + position + ", unitWidth=" + unitWidth + ", errorCorrection=" + errorCorrection + ", data="
                + data + "]";
    }

}
