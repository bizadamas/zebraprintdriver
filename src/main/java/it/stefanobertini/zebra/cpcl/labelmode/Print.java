package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the PRINT command<br/>
 * <br/>
 * Description:<br>
 * The PRINT command terminates and prints the file. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * PRINT
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Print extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "PRINT";
    }

    @Override
    public void validate() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Print []";
    }

}
