package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.Size;
import it.stefanobertini.zebra.enums.Orientation;

public class ExpandedGraphics extends CompressedGraphics {

    public ExpandedGraphics(Orientation orientation, Size size, Position position, String data) {
        super(orientation, size, position, data);
    }

    public ExpandedGraphics(Orientation orientation, Size size, Position position, int[] data) {
        super(orientation, size, position, data);
    }

    public String getCommand() {
        return Orientation.vertical.equals(getOrientation()) ? "VEXPANDED-GRAPHICS" : "EXPANDED-GRAPHICS";
    }

    @Override
    public String toString() {
        return "ExpandedGraphics [getOrientation()=" + getOrientation() + ", getSize()=" + getSize() + ", getPosition()=" + getPosition() + ", getData()="
                + getData() + "]";
    }

}
