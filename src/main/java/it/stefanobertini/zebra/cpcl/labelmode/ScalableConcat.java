package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandTextProducerInterface;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.ScalableConcatBitmapText;
import it.stefanobertini.zebra.beans.ScalableConcatScalableText;
import it.stefanobertini.zebra.enums.Orientation;

import java.util.ArrayList;
import java.util.List;

public class ScalableConcat extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation;
    private Position position;
    private List<CommandTextProducerInterface> textList = new ArrayList<CommandTextProducerInterface>();

    public ScalableConcat(Orientation orientation) {
        this(orientation, new Position());
    }

    public ScalableConcat(Orientation orientation, Position position) {
        super();
        this.orientation = orientation;
        this.position = position;
    }

    public void addScalableText(String name, int fontWidth, int fontHeight, double offset, String text) {
        textList.add(new ScalableConcatScalableText(name, fontWidth, fontHeight, offset, text));
    }

    public void addBitmapText(Font font, double offset, String text) {
        textList.add(new ScalableConcatBitmapText(font, offset, text));
    }

    public String getCommand() {
        return Orientation.vertical.equals(orientation) ? "VCONCAT" : "CONCAT";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        endLine();

        for (CommandTextProducerInterface concatText : textList) {
            appendText(concatText.getCommandLine());
        }

        appendText("ENDCONCAT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isNotEmpty("textList", textList);
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<CommandTextProducerInterface> getTextList() {
        return textList;
    }

    public void setTextList(List<CommandTextProducerInterface> textList) {
        this.textList = textList;
    }

    @Override
    public String toString() {
        return "ScalableConcat [orientation=" + orientation + ", position=" + position + ", textList=" + textList + "]";
    }
}
