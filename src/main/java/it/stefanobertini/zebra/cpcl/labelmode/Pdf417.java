package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

import java.util.ArrayList;
import java.util.List;

public class Pdf417 extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation = Orientation.horizontal;
    private Position position;
    private double unitWidth;
    private double unitHeight;
    private int columnNumber;
    private int securityLevel;
    private List<String> data;

    public Pdf417(Orientation orientation, Position position, double unitWidth, double unitHeight, int columnNumber, int securityLevel) {
        this(orientation, position, unitWidth, unitHeight, columnNumber, securityLevel, new ArrayList<String>());
    }

    public Pdf417(Orientation orientation, Position position, double unitWidth, double unitHeight, int columnNumber, int securityLevel, List<String> data) {
        super();
        this.orientation = orientation;
        this.position = position;
        this.unitWidth = unitWidth;
        this.unitHeight = unitHeight;
        this.columnNumber = columnNumber;
        this.securityLevel = securityLevel;
        this.data = data;
    }

    public void addData(String data) {
        this.data.add(data);
    }

    public String getCommand() {
        return (Orientation.vertical.equals(orientation) ? "VBARCODE" : "BARCODE") + " PDF-417";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText("XD");
        appendText(" ");
        appendText(unitWidth);
        appendText(" ");
        appendText("YD");
        appendText(" ");
        appendText(unitHeight);
        appendText(" ");
        appendText("C");
        appendText(" ");
        appendText(columnNumber);
        appendText(" ");
        appendText("S");
        appendText(" ");
        appendText(securityLevel);
        endLine();

        for (int i = 0; data != null && i < data.size(); i++) {
            String item = data.get(i);
            appendText(item);
            endLine();
        }

        appendText("ENDPDF");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isNotEmpty("data", data);
        Validator.isBetween("unitWidth", unitWidth, 1, 32);
        Validator.isBetween("unitHeight", unitHeight, 1, 32);
        Validator.isBetween("securityLevel", securityLevel, 0, 8);
        Validator.isBetween("columnNumber", columnNumber, 1, 30);
    }

    /**
     * @return the orientation
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the unitWidth
     */
    public double getUnitWidth() {
        return unitWidth;
    }

    /**
     * @param unitWidth
     *            the unitWidth to set
     */
    public void setUnitWidth(double unitWidth) {
        this.unitWidth = unitWidth;
    }

    /**
     * @return the unitHeight
     */
    public double getUnitHeight() {
        return unitHeight;
    }

    /**
     * @param unitHeight
     *            the unitHeight to set
     */
    public void setUnitHeight(double unitHeight) {
        this.unitHeight = unitHeight;
    }

    /**
     * @return the columnNumber
     */
    public int getColumnNumber() {
        return columnNumber;
    }

    /**
     * @param columnNumber
     *            the columnNumber to set
     */
    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    /**
     * @return the securityLevel
     */
    public int getSecurityLevel() {
        return securityLevel;
    }

    /**
     * @param securityLevel
     *            the securityLevel to set
     */
    public void setSecurityLevel(int securityLevel) {
        this.securityLevel = securityLevel;
    }

    /**
     * @return the data
     */
    public List<String> getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(List<String> data) {
        this.data = data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Pdf417 [orientation=" + orientation + ", position=" + position + ", unitWidth=" + unitWidth + ", unitHeight=" + unitHeight + ", columnNumber="
                + columnNumber + ", securityLevel=" + securityLevel + ", data=" + data + "]";
    }

}
