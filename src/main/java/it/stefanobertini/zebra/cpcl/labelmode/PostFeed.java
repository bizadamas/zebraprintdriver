package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class PostFeed extends AbstractDoubleParameterCommand implements LabelModeCommandInterface {

    public PostFeed() {
        this(0);
    }

    public PostFeed(double length) {
        setParameter(length);
    }

    public String getCommand() {
        return "POSTFEED";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("length", getParameter(), 0);
    }

    public double getLength() {
        return getParameter();
    }

    public void setLength(double length) {
        setParameter(length);
    }

    @Override
    public String toString() {
        return "PostFeed [length=" + getParameter() + "]";
    }

}
