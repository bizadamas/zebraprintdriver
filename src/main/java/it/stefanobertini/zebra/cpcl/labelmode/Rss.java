package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.RssBarcodeType;

public class Rss extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation = Orientation.horizontal;
    private Position position;
    private double width;
    private double linearWidth;
    private double separatorHeight;
    private int segments;
    private String subtype;
    private String linearData;
    private String bidiData;

    public Rss(Orientation orientation, Position position, double width, double linearWidth, double separatorHeight, int segments, RssBarcodeType subtype,
            String linearData, String bidiData) {
        this(orientation, position, width, linearWidth, separatorHeight, segments, subtype.getCode(), linearData, bidiData);
    }

    public Rss(Orientation orientation, Position position, double width, double linearWidth, double separatorHeight, int segments, String subtype,
            String linearData, String bidiData) {
        super();
        this.orientation = orientation;
        this.position = position;
        this.width = width;
        this.linearWidth = linearWidth;
        this.separatorHeight = separatorHeight;
        this.segments = segments;
        this.subtype = subtype;
        this.linearData = linearData;
        this.bidiData = bidiData;
    }

    public String getCommand() {
        return (Orientation.vertical.equals(orientation) ? "VBARCODE" : "BARCODE") + " RSS";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(width);
        appendText(" ");
        appendText(linearWidth);
        appendText(" ");
        appendText(separatorHeight);
        appendText(" ");
        appendText(segments);
        appendText(" ");
        appendText(subtype);
        appendText(" ");
        appendText(linearData);
        if (bidiData != null && bidiData.length() > 0) {
            appendText("|");
            appendText(bidiData);
        }
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isRequired("subtype", subtype);
        Validator.isRequired("linearData", linearData);
        Validator.isMoreThanOrEqualTo("width", width, 0);
        Validator.isMoreThanOrEqualTo("linearWidth", linearWidth, 0);
        Validator.isMoreThanOrEqualTo("separatorHeight", separatorHeight, 0);
        Validator.isMoreThanOrEqualTo("segments", segments, 0);
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the linearWidth
     */
    public double getLinearWidth() {
        return linearWidth;
    }

    /**
     * @param linearWidth
     *            the linearWidth to set
     */
    public void setLinearWidth(double linearWidth) {
        this.linearWidth = linearWidth;
    }

    /**
     * @return the separatorHeight
     */
    public double getSeparatorHeight() {
        return separatorHeight;
    }

    /**
     * @param separatorHeight
     *            the separatorHeight to set
     */
    public void setSeparatorHeight(double separatorHeight) {
        this.separatorHeight = separatorHeight;
    }

    /**
     * @return the segments
     */
    public int getSegments() {
        return segments;
    }

    /**
     * @param segments
     *            the segments to set
     */
    public void setSegments(int segments) {
        this.segments = segments;
    }

    /**
     * @return the subtype
     */
    public String getSubtype() {
        return subtype;
    }

    /**
     * @param subtype
     *            the subtype to set
     */
    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    /**
     * @return the linearData
     */
    public String getLinearData() {
        return linearData;
    }

    /**
     * @param linearData
     *            the linearData to set
     */
    public void setLinearData(String linearData) {
        this.linearData = linearData;
    }

    /**
     * @return the bidiData
     */
    public String getBidiData() {
        return bidiData;
    }

    /**
     * @param bidiData
     *            the bidiData to set
     */
    public void setBidiData(String bidiData) {
        this.bidiData = bidiData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Rss [position=" + position + ", width=" + width + ", linearWidth=" + linearWidth + ", separatorHeight=" + separatorHeight + ", segments="
                + segments + ", subtype=" + subtype + ", linearData=" + linearData + ", bidiData=" + bidiData + "]";
    }

}
