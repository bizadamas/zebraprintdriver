package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the END command<br/>
 * <br/>
 * Description:<br>
 * The END command properly terminates a command and executes it without
 * printing. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * END
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class End extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "END";
    }

    @Override
    public void validate() {
    }
}
