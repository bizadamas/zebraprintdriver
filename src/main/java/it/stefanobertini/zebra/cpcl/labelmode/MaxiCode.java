package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.MaxiCodeTag;

import java.util.ArrayList;
import java.util.List;

public class MaxiCode extends AbstractCommand implements LabelModeCommandInterface {

    private static class MaxiCodeTagData {
        private String tag;
        private String options;

        public MaxiCodeTagData(String tag, String options) {
            super();
            this.tag = tag;
            this.options = options;
        }

        /**
         * @return the tag
         */
        public String getTag() {
            return tag;
        }

        /**
         * @return the options
         */
        public String getOptions() {
            return options;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "MaxiCodeTagData [tag=" + tag + ", options=" + options + "]";
        }

    }

    private Position position;
    private List<MaxiCodeTagData> tags = new ArrayList<MaxiCode.MaxiCodeTagData>();

    public MaxiCode(Position position) {
        super();
        this.position = position;
    }

    public void addTag(MaxiCodeTag tag, String options) {
        addTag(tag.getCode(), options);
    }

    public void addTag(String tag, String options) {
        MaxiCodeTagData maxiCodeTagData = new MaxiCodeTagData(tag, options);
        tags.add(maxiCodeTagData);
    }

    public String getCommand() {
        return "BARCODE MAXICODE";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        endLine();

        for (MaxiCodeTagData tag : tags) {
            appendText(tag.getTag());
            appendText(" ");
            appendText(tag.getOptions());
            endLine();
        }
        appendText("ENDMAXICODE");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("position", position);
        Validator.isNotEmpty("tags", tags);
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the tags
     */
    public List<MaxiCodeTagData> getTags() {
        return tags;
    }

    /**
     * @param tags
     *            the tags to set
     */
    public void setTags(List<MaxiCodeTagData> tags) {
        this.tags = tags;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MaxiCode [position=" + position + ", tags=" + tags + "]";
    }

}
