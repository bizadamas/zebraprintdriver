package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class PartialCut extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "PARTIAL-CUT";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "PartialCut []";
    }
}
