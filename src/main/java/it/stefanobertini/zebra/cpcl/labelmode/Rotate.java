package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class Rotate extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Rotate() {
        this(0);
    }

    public Rotate(int angle) {
        setParameter(angle);
    }

    @Override
    public void validate() {
        Validator.isBetween("angle", getParameter(), 0, 360);
    }

    public String getCommand() {
        return "ROTATE";
    }

    public int getAngle() {
        return getParameter();
    }

    public void setAngle(int angle) {
        setParameter(angle);
    }

    @Override
    public String toString() {
        return "Rotate [angle=" + getParameter() + "]";
    }

}
