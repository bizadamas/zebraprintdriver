package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.JustificationType;

public class Justification extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    private JustificationType justificationType;

    public Justification() {
        this(JustificationType.left);
    }

    public Justification(JustificationType justificationType) {
        super();
        this.justificationType = justificationType;
    }

    public String getCommand() {
        return justificationType.getCode();
    }

    @Override
    public void validate() {
        Validator.isRequired("justificationType", justificationType);
    }

    /**
     * @return the justificationType
     */
    public JustificationType getJustificationType() {
        return justificationType;
    }

    /**
     * @param justificationType
     *            the justificationType to set
     */
    public void setJustificationType(JustificationType justificationType) {
        this.justificationType = justificationType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Justification [justificationType=" + justificationType + "]";
    }

}
