package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

public class ScaleToFit extends ScaleText {

    public ScaleToFit(String name, int fontWidth, int fontHeight) {
        super(Orientation.horizontal, name, fontWidth, fontHeight, new Position(0, 0), "");
    }

    public ScaleToFit(Orientation orientation, String name, int fontWidth, int fontHeight, Position position, String text) {
        super(orientation, name, fontWidth, fontHeight, position, text);
    }

    public String getCommand() {
        return Orientation.vertical.equals(getOrientation()) ? "VSCALE-TO-FIT" : "SCALE-TO-FIT";
    }

    @Override
    public String toString() {
        return "ScaleToFit [getOrientation()=" + getOrientation() + ", getName()=" + getName() + ", getFontWidth()=" + getFontWidth() + ", getFontHeight()="
                + getFontHeight() + ", getPosition()=" + getPosition() + ", getText()=" + getText() + "]";
    }

}
