package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetLinePrintTimeout extends AbstractIntegerParameterCommand implements LineModeCommandInterface {

    public SetLinePrintTimeout() {
        this(0);
    }

    public SetLinePrintTimeout(int timeout) {
        setParameter(timeout);
    }

    public String getCommand() {
        return "! U1 SETLP-TIMEOUT";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("timeout", getParameter(), 0);
    }

    public void setTimeout(int timeout) {
        setParameter(timeout);
    }

    public int getTimeout() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetLinePrintTimeout [timeout=" + getParameter() + "]";
    }
}
