package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class PageHeight extends AbstractDoubleParameterCommand implements LineModeCommandInterface {

    public PageHeight() {
        setParameter(0);
    }

    public PageHeight(double height) {
        setParameter(height);
    }

    public String getCommand() {
        return "! U1 PH";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("height", getParameter(), 0);
    }

    public void setHeight(double height) {
        setParameter(height);
    }

    public double getHeight() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PageHeight [height=" + getParameter() + "]";
    }

}
