package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.UnitsType;

public class Units extends AbstractCommand implements LineModeCommandInterface {

    private UnitsType unitsType;

    public Units() {
        this(UnitsType.dots);
    }

    public Units(UnitsType unitsType) {
        this.unitsType = unitsType;
    }

    public String getCommand() {
        return unitsType.getCode();
    }

    @Override
    protected void getCommandLineInternal() {
        appendText("! U");
        endLine();
        appendText(getCommand());
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("unitsType", unitsType);
    }

    /**
     * @return the unitsType
     */
    public UnitsType getUnitsType() {
        return unitsType;
    }

    /**
     * @param unitsType
     *            the unitsType to set
     */
    public void setUnitsType(UnitsType unitsType) {
        this.unitsType = unitsType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Units [unitsType=" + unitsType + "]";
    }

}
