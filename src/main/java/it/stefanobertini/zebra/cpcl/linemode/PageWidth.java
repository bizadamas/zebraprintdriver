package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class PageWidth extends AbstractDoubleParameterCommand implements LineModeCommandInterface {

    public PageWidth() {
        setParameter(0);
    }

    public PageWidth(double width) {
        setParameter(width);
    }

    public String getCommand() {
        return "! U1 PW";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("width", getParameter(), 0);
    }

    public void setWidth(double width) {
        setParameter(width);
    }

    public double getWidth() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PageWidth [width=" + getParameter() + "]";
    }

}
