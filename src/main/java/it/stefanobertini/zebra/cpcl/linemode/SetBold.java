package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetBold extends AbstractDoubleParameterCommand implements LineModeCommandInterface {

    public SetBold() {
        setParameter(0);
    }

    public SetBold(double boldness) {
        setParameter(boldness);
    }

    public String getCommand() {
        return "! U1 SETBOLD";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("boldness", getParameter(), 0);
    }

    public void setBoldness(double boldness) {
        setParameter(boldness);
    }

    public double getBoldness() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetBold [boldness=" + getParameter() + "]";
    }

}
