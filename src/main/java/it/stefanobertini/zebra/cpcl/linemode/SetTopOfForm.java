package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetTopOfForm extends AbstractCommand implements LineModeCommandInterface {

    private double distance;

    public SetTopOfForm() {
        this(0);
    }

    public SetTopOfForm(double distance) {
        super();
        this.distance = distance;
    }

    public String getCommand() {
        return "SET-TOF";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText("! U");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(distance);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("distance", distance, 0);
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "SetTopOfForm [distance=" + distance + "]";
    }

}
