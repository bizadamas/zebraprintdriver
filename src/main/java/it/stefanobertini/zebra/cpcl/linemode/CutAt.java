package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class CutAt extends AbstractCommand implements LineModeCommandInterface {

    private double distance;
    private boolean useDefaultPrinterValue = false;

    public CutAt() {
        this(0, true);
    }

    public CutAt(double distance) {
        this(distance, false);
    }

    private CutAt(double distance, boolean useDefaultPrinterValue) {
        super();
        this.distance = distance;
        this.useDefaultPrinterValue = useDefaultPrinterValue;
    }

    public String getCommand() {
        return "! U1 CUT-AT";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        if (!useDefaultPrinterValue) {
            appendText(" ");
            appendText(distance);
        }
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("length", distance, 0);
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
        setUseDefaultPrinterValue(false);
    }

    public boolean isUseDefaultPrinterValue() {
        return useDefaultPrinterValue;
    }

    public void setUseDefaultPrinterValue(boolean useDefaultPrinterValue) {
        this.useDefaultPrinterValue = useDefaultPrinterValue;
    }

    @Override
    public String toString() {
        return "CutAt [distance=" + distance + ", useDefaultPrinterValue=" + useDefaultPrinterValue + "]";
    }

}
