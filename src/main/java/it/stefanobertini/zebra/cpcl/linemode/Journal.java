package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;

public class Journal extends AbstractNoParameterCommand implements LineModeCommandInterface {

    public String getCommand() {
        return "! U1 JOURNAL";
    }

    @Override
    public void validate() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EndPage []";
    }

}
