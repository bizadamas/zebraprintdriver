package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class LeftMargin extends AbstractDoubleParameterCommand implements LineModeCommandInterface {

    public LeftMargin() {
        setParameter(0);
    }

    public LeftMargin(double margin) {
        setParameter(margin);
    }

    public String getCommand() {
        return "! U1 LMARGIN";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("margin", getParameter(), 0);
    }

    public void setMargin(double margin) {
        setParameter(margin);
    }

    public double getMargin() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LeftMargin [margin=" + getParameter() + "]";
    }

}
