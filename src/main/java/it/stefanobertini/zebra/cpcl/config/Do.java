package it.stefanobertini.zebra.cpcl.config;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.ConfigurationKey;

public class Do extends AbstractCommand implements CommandInterface {

    private ConfigurationKey configurationKey;
    private String value;

    public Do(ConfigurationKey configurationKey) {
        this(configurationKey, "");
    }

    public Do(ConfigurationKey configurationKey, String value) {
        super();
        this.configurationKey = configurationKey;
        this.value = value;

        if (!configurationKey.isDoAction()) {
            Validator.signalError("Command " + configurationKey + " (" + configurationKey.getCode() + ") is not a do command.");
        }
    }

    public String getCommand() {
        return "! U1 do";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText("\"");
        appendText(configurationKey.getCode());
        appendText("\"");
        appendText(" ");
        appendText("\"");
        appendText(value);
        appendText("\"");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("configurationKey", configurationKey);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetVar [configurationKey=" + configurationKey + ", value=" + value + "]";
    }

}
