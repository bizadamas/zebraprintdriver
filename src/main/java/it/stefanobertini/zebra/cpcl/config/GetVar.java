package it.stefanobertini.zebra.cpcl.config;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.ConfigurationKey;

public class GetVar extends AbstractCommand implements CommandInterface {

    private ConfigurationKey configurationKey;

    public GetVar(ConfigurationKey configurationKey) {
        super();
        this.configurationKey = configurationKey;

        if (!configurationKey.isGetVarAction()) {
            Validator.signalError("Command " + configurationKey + " (" + configurationKey.getCode() + ") is not a getvar command.");
        }
    }

    public String getCommand() {
        return "! U1 getvar";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText("\"");
        appendText(configurationKey.getCode());
        appendText("\"");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("configurationKey", configurationKey);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "GetVar [configurationKey=" + configurationKey + "]";
    }

}
