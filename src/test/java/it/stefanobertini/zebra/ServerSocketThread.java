package it.stefanobertini.zebra;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketThread extends Thread {

    private ServerSocket serverSocket;
    private byte[] received;
    private byte[] answer;
    private long delay;

    private boolean finished = false;
    private final static int BUFFER_SIZE = 2048;

    public ServerSocketThread(ServerSocket serverSocket) {
	this(serverSocket, null, 0);
    }

    public ServerSocketThread(ServerSocket serverSocket, byte[] answer, long delay) {
	super();
	this.serverSocket = serverSocket;
	this.answer = answer;
	this.delay = delay;
    }

    public byte[] getReceived() {
	return received;
    }

    public boolean isFinished() {
	return finished;
    }

    public void run() {
	Socket socket = null;
	InputStream inputStream = null;
	OutputStream outputStream = null;

	ByteArrayOutputStream bos;

	try {
	    socket = serverSocket.accept();

	    inputStream = socket.getInputStream();
	    outputStream = socket.getOutputStream();

	    bos = new ByteArrayOutputStream();

	    while (inputStream.available() == 0) {
		Thread.sleep(100);
	    }

	    while (inputStream.available() > 0) {
		byte[] byteBuffer = new byte[BUFFER_SIZE];
		int bytesRead = inputStream.read(byteBuffer, 0, BUFFER_SIZE);
		bos.write(byteBuffer, 0, bytesRead);
	    }

	    received = bos.toByteArray();

	    if (answer != null && answer.length > 0) {
		Thread.sleep(delay);

		outputStream.write(answer);
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (inputStream != null) {
		try {
		    inputStream.close();
		} catch (IOException e) {
		}
	    }
	    if (outputStream != null) {
		try {
		    outputStream.close();
		} catch (IOException e) {
		}
	    }
	    if (socket != null) {
		try {
		    socket.close();
		} catch (IOException e) {
		}
	    }
	}
	finished = true;

    }

}
