package it.stefanobertini.zebra;

import it.stefanobertini.zebra.CommandInterface;

public class CommandAssert {

    protected CommandAssert() {
    }

    static public void assertCommand(String message, CommandOutputBuilder expected, CommandInterface command) {
	org.junit.Assert.assertEquals(message, expected.getCommandText(), new String(command.getCommandByteArray()));
    }

    static public void assertCommand(CommandOutputBuilder expected, CommandInterface command) {
	assertCommand(command.getCommand(), expected, command);
    }

}