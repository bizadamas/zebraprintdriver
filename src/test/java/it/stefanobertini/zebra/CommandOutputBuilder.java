package it.stefanobertini.zebra;

import it.stefanobertini.zebra.FormatUtils;

public class CommandOutputBuilder {

    private StringBuffer commandText;

    public CommandOutputBuilder() {
	commandText = new StringBuffer();
    }

    public CommandOutputBuilder(String... text) {
	this();
	for (int i = 0; text != null && i < text.length; i++) {
	    printLn(text[i]);
	}
    }

    public String getCommandText() {
	return commandText.toString();
    }

    public void print(String text) {
	commandText.append(text);
    }

    public void print(int text) {
	commandText.append("" + text);
    }

    public void print(double text) {
	commandText.append(FormatUtils.format(text));
    }

    public void printLn(String text) {
	commandText.append(text);
	commandText.append("\r\n");
    }

    @Override
    public String toString() {
	return "CommandOutputBuilder [commandText=" + commandText + "]";
    }

}
