package it.stefanobertini.zebra;

import static org.junit.Assert.assertEquals;

import it.stefanobertini.zebra.Validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ValidatorTest {

    @Test
    public void testSignalError() {

	try {
	    Validator.signalError("TEST");
	} catch (Exception e) {
	    String message = e.getMessage();

	    assertEquals("TEST", message);
	}
    }

    @Test
    public void testIsMoreThanOk() {
	Validator.isMoreThan("TEST", 1, 0);
    }

    @Test
    public void testIsMoreThanBad() {
	try {
	    Validator.isMoreThan("TEST", 1, 5);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 1. Should be > 5";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsLessThanOrEqualToOk() {
	Validator.isLessThanOrEqualTo("TEST", 0, 0);
    }

    @Test
    public void testIsLessThanOrEqualToBad() {
	try {
	    Validator.isLessThanOrEqualTo("TEST", 5, 1);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 5. Should be <= 1";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsLessThanOk() {
	Validator.isLessThan("TEST", 1, 0);
    }

    @Test
    public void testIsLessThanBad() {
	try {
	    Validator.isLessThan("TEST", 5, 1);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 5. Should be < 1";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsMoreThanOrEqualToOk() {
	Validator.isMoreThanOrEqualTo("TEST", 0, 0);
    }

    @Test
    public void testIsMoreThanOrEqualToBad() {
	try {
	    Validator.isMoreThanOrEqualTo("TEST", 1, 5);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 1. Should be >= 5";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsBetweenOk() {
	Validator.isBetween("TEST", 3, 0, 5);
    }

    @Test
    public void testIsBetweenErrorLess() {
	try {
	    Validator.isBetween("TEST", 0, 1, 5);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 0. Should be between 1 and 5";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsBetweenErrorMore() {
	try {
	    Validator.isBetween("TEST", 6, 1, 5);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is 6. Should be between 1 and 5";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsRequiredStringStringOk() {
	Validator.isRequired("TEST", "TEST");
    }

    @Test
    public void testIsRequiredStringStringBadEmpty() {
	try {
	    Validator.isRequired("TEST", "");
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is mandatory.";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsRequiredStringStringBadNull() {
	try {
	    Validator.isRequired("TEST", null);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is mandatory.";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsRequiredStringObject() {
	try {
	    Integer test = null;
	    Validator.isRequired("TEST", test);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is mandatory.";
	    assertEquals(expected, message);
	}

    }

    @Test
    public void testIsNotEmptyOk() {
	List<String> test = new ArrayList<String>();
	test.add("Not empty");
	Validator.isNotEmpty("TEST", test);
    }

    @Test
    public void testIsNotEmptyBadEmpry() {
	List<String> test = new ArrayList<String>();

	try {
	    Validator.isNotEmpty("TEST", test);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is mandatory.";
	    assertEquals(expected, message);
	}
    }

    @Test
    public void testIsNotEmptyBadNull() {
	List<String> test = null;

	try {
	    Validator.isNotEmpty("TEST", test);
	} catch (Exception e) {
	    String message = e.getMessage();
	    String expected = "Parameter TEST is mandatory.";
	    assertEquals(expected, message);
	}
    }
}
