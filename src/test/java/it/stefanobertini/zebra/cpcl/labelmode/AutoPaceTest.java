package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.AutoPace;

import org.junit.Test;

public class AutoPaceTest {

    @Test
    public void test() {
	AutoPace command = new AutoPace();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("AUTO-PACE");

	assertCommand(output, command);
    }

    public void fakeTest() {
	AutoPace command = new AutoPace();

	command.toString();
    }
}
