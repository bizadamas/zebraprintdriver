package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Pdf417;
import it.stefanobertini.zebra.enums.Orientation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class Pdf417Test {

    @Test
    public void test() {

	List<String> data = new ArrayList<String>();
	data.add("PDF Data");
	data.add("ABCDE12345");

	Pdf417 command = new Pdf417(Orientation.horizontal, new Position(10, 20), 3, 12, 3, 2, data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE PDF-417 10 20 XD 3 YD 12 C 3 S 2");
	output.printLn("PDF Data");
	output.printLn("ABCDE12345");
	output.printLn("ENDPDF");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {

	Pdf417 command = new Pdf417(Orientation.horizontal, new Position(10, 20), 3, 12, 3, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("");

	assertCommand(output, command);
    }

}
