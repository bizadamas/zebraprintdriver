package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Pattern;
import it.stefanobertini.zebra.enums.PatternType;

import org.junit.Test;

public class PatternTest {

    @Test
    public void testString() {
	Pattern command = new Pattern("106");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PATTERN 106");

	assertCommand(output, command);
    }

    @Test
    public void testEnum() {
	Pattern command = new Pattern(PatternType.crossHatckPattern);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PATTERN 106");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Pattern command = new Pattern();

	command.getCommandByteArray();
    }
}
