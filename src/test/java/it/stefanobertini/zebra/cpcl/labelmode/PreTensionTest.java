package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PreTension;

import org.junit.Test;

public class PreTensionTest {

    @Test
    public void testDefault() {
	PreTension command = new PreTension();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PRE-TENSION 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	PreTension command = new PreTension(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PRE-TENSION 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PreTension command = new PreTension(-1);

	command.getCommandByteArray();
    }

}
