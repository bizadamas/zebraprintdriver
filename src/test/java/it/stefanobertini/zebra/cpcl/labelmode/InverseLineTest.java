package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.InverseLine;

import org.junit.Test;

public class InverseLineTest {

    @Test
    public void testDefault() {
	InverseLine command = new InverseLine(new Position(1, 2), new Position(3, 4));

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("INVERSE-LINE 1 2 3 4 1");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	InverseLine command = new InverseLine(new Position(1, 2), new Position(3, 4), 5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("INVERSE-LINE 1 2 3 4 5");

	assertCommand(output, command);
    }

}
