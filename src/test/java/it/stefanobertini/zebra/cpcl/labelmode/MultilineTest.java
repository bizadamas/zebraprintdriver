package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Multiline;
import it.stefanobertini.zebra.enums.TextRotation;

import org.junit.Test;

public class MultilineTest {

    @Test
    public void test() {

	Multiline command = new Multiline(47, TextRotation.horizontal, new Font("4", 0), new Position(10, 20));
	command.addText("1st line of text");
	command.addText("2nd line of text");
	command.addText(":");
	command.addText("Nth line of text");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("MULTILINE 47");
	output.printLn("TEXT 4 0 10 20");
	output.printLn("1st line of text");
	output.printLn("2nd line of text");
	output.printLn(":");
	output.printLn("Nth line of text");
	output.printLn("ENDMULTILINE");

	assertCommand(output, command);
    }

}
