package it.stefanobertini.zebra.cpcl.labelmode;

import static org.junit.Assert.assertArrayEquals;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Pcx;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.junit.Test;

public class PcxTest {

    @Test
    public void testFromPrinter() {
	Pcx command = new Pcx(new Position(1, 2));
	command.loadImageFromPrinter("FILE.TXT");

	byte[] data = command.getCommandByteArray();
	byte[] expected = "PCX 1 2 !<FILE.TXT\r\n".getBytes();

	assertArrayEquals(expected, data);
    }

    @Test
    public void testFromFile() throws Exception {
	Pcx command = new Pcx(new Position(1, 2));
	command.loadImageFromFile(new File("./target/test-classes/test.pcx"));

	byte[] data = command.getCommandByteArray();
	ByteArrayOutputStream expected = new ByteArrayOutputStream();
	expected.write("PCX 1 2\r\n".getBytes());
	for (byte i = 0; i < 32; i++) {
	    expected.write(i);
	}
	expected.write("\r\n".getBytes());

	assertArrayEquals(expected.toByteArray(), data);
    }

    @Test
    public void testFromResource() throws Exception {
	Pcx command = new Pcx(new Position(1, 2));
	command.loadImageFromResource("test.pcx");

	byte[] data = command.getCommandByteArray();
	ByteArrayOutputStream expected = new ByteArrayOutputStream();
	expected.write("PCX 1 2\r\n".getBytes());
	for (byte i = 0; i < 32; i++) {
	    expected.write(i);
	}
	expected.write("\r\n".getBytes());

	assertArrayEquals(expected.toByteArray(), data);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Pcx command = new Pcx(new Position(1, 2));

	command.getCommandByteArray();
    }

}
