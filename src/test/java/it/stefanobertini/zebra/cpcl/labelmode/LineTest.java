package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Line;

import org.junit.Test;

public class LineTest {

    @Test
    public void testDefault() {
	Line command = new Line(new Position(1, 2), new Position(3, 4));

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("LINE 1 2 3 4 1");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Line command = new Line(new Position(1, 2), new Position(3, 4), 5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("LINE 1 2 3 4 5");

	assertCommand(output, command);
    }

}
