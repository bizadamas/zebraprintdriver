package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Comment;

import org.junit.Test;

public class CommentTest {

    @Test
    public void test() {
	Comment command = new Comment("This is a comment.");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("; This is a comment.");

	assertCommand(output, command);
    }

}
