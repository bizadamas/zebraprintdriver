package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.cpcl.labelmode.BarcodeText;

import org.junit.Test;

public class BarcodeTextTest {

    @Test
    public void test() {

	BarcodeText command = new BarcodeText(new Font("4", 0), 10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE-TEXT 4 0 10");

	assertCommand(output, command);
    }
}
