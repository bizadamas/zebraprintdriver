package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Abort;

import org.junit.Test;

public class AbortTest {

    @Test
    public void test() {
	Abort command = new Abort();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ABORT");

	assertCommand(output, command);
    }

    @Test
    public void fakeTest() {
	Abort command = new Abort();

	command.toString();
    }

}
