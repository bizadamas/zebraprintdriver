package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Speed;

import org.junit.Test;

public class SpeedTest {

    public void testDefault() {
	Speed command = new Speed();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SPEED 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Speed command = new Speed(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SPEED 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Speed command = new Speed(6);

	command.getCommandByteArray();
    }

}
