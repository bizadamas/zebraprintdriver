package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Rewind;
import it.stefanobertini.zebra.enums.OnOffMode;

import org.junit.Test;

public class RewindTest {

    @Test
    public void testDefault() {
	Rewind command = new Rewind();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("REWIND-ON");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Rewind command = new Rewind(OnOffMode.off);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("REWIND-OFF");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidate() {
	Rewind command = new Rewind(null);
	command.getCommandByteArray();
    }

}
