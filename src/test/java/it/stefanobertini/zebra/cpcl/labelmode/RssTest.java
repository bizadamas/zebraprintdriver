package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Rss;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.RssBarcodeType;

import org.junit.Test;

public class RssTest {

    @Test
    public void test1() {
	Rss command = new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14, "1234567890123", "1234567890");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE RSS 10 110 2 25 3 22 1 1234567890123|1234567890");

	assertCommand(output, command);
    }

    @Test
    public void test2() {
	Rss command = new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14_Stacked, "1234567890123", "");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE RSS 10 110 2 25 3 22 3 1234567890123");

	assertCommand(output, command);
    }

    @Test
    public void test3() {
	Rss command = new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_Expanded, "1234567890123", "");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE RSS 10 110 2 25 3 22 6 1234567890123");

	assertCommand(output, command);
    }

    @Test
    public void test4() {
	Rss command = new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.UCC_128_Composite_A_B, "12345678901234567890",
	        "1234567890");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE RSS 10 110 2 25 3 22 11 12345678901234567890|1234567890");

	assertCommand(output, command);
    }

    @Test
    public void test5() {
	Rss command = new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14, "1011234567890", "");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE RSS 10 110 2 25 3 22 1 1011234567890");

	assertCommand(output, command);
    }

}
