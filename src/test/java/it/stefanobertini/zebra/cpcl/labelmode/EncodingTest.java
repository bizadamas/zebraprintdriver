package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Encoding;
import it.stefanobertini.zebra.enums.EncodingType;

import org.junit.Test;

public class EncodingTest {

    @Test
    public void test() {
	Encoding command = new Encoding(EncodingType.ascii);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ENCODING ASCII");

	assertCommand(output, command);
    }

    @Test
    public void testWithString() {
	Encoding command = new Encoding("ASCII");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ENCODING ASCII");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Encoding command = new Encoding();

	command.getCommandByteArray();
    }

}
