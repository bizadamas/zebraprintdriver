package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Country;
import it.stefanobertini.zebra.enums.CountryCode;

import org.junit.Test;

public class CountryTest {

    @Test
    public void test() {
	Country command = new Country(CountryCode.italy);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("COUNTRY ITALY");

	assertCommand(output, command);
    }

    @Test
    public void testWithString() {
	Country command = new Country("ITALY");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("COUNTRY ITALY");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	Country command = new Country();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("COUNTRY USA");

	assertCommand(output, command);
    }

}
