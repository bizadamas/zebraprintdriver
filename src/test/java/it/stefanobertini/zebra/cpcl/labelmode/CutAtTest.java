package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.CutAt;

import org.junit.Test;

public class CutAtTest {

    @Test
    public void test() {
	CutAt command = new CutAt(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CUT-AT 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	CutAt command = new CutAt();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CUT-AT 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	CutAt command = new CutAt(-1);

	command.getCommandByteArray();
    }
}
