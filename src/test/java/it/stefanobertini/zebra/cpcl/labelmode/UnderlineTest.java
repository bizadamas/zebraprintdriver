package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Underline;
import it.stefanobertini.zebra.enums.OnOffMode;

import org.junit.Test;

public class UnderlineTest {

    @Test
    public void testDefault() {
	Underline command = new Underline();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("UNDERLINE ON");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Underline command = new Underline(OnOffMode.off);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("UNDERLINE OFF");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Underline command = new Underline(null);

	command.getCommandByteArray();
    }

}
