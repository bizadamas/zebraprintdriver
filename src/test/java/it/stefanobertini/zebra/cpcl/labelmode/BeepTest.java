package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Beep;

import org.junit.Test;

public class BeepTest {

    @Test
    public void test1() {

	Beep command = new Beep();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BEEP 0");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	Beep command = new Beep(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BEEP 10");

	assertCommand(output, command);
    }
}
