package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Contrast;
import it.stefanobertini.zebra.enums.ContrastLevel;

import org.junit.Test;

public class ContrastTest {

    @Test
    public void testNumber() {
	Contrast command = new Contrast(2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CONTRAST 2");

	assertCommand(output, command);
    }

    @Test
    public void testEnum() {
	Contrast command = new Contrast(ContrastLevel.dark);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CONTRAST 2");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	Contrast command = new Contrast();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CONTRAST 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	Contrast command = new Contrast(-1);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	Contrast command = new Contrast(4);

	command.getCommandByteArray();
    }
}
