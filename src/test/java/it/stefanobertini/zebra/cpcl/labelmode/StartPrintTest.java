package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;

import org.junit.Test;

public class StartPrintTest {

    @Test
    public void test() {
	StartPrint command = new StartPrint(1.1, 2, 3, 4.4, 5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! 1.1 2 3 4.4 5");

	assertCommand(output, command);
    }

}
