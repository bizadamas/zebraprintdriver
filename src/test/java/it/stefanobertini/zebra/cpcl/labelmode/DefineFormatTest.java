package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.DefineFormat;

import org.junit.Test;

public class DefineFormatTest {

    @Test
    public void test() {
	DefineFormat command = new DefineFormat("TEST.FMT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! DEFINE-FORMAT TEST.FMT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	DefineFormat command = new DefineFormat();

	command.getCommandByteArray();

    }
}
