package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PostFeed;

import org.junit.Test;

public class PostFeedTest {

    @Test
    public void testDefault() {
	PostFeed command = new PostFeed();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("POSTFEED 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	PostFeed command = new PostFeed(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("POSTFEED 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PostFeed command = new PostFeed(-1);

	command.getCommandByteArray();
    }

}
