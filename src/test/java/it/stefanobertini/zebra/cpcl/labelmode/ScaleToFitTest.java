package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.ScaleToFit;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class ScaleToFitTest {

    @Test
    public void test() {
	ScaleToFit command = new ScaleToFit(Orientation.horizontal, "PLL_LAT.CSF", 40, 10, new Position(0, 10), "SALE");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SCALE-TO-FIT PLL_LAT.CSF 40 10 0 10 SALE");

	assertCommand(output, command);
    }

}
