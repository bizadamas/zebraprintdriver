package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.UseFormat;

import org.junit.Test;

public class UseFormatTest {

    @Test
    public void test() {
	UseFormat command = new UseFormat("TEST.FMT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! USE-FORMAT TEST.FMT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	UseFormat command = new UseFormat();

	command.getCommandByteArray();

    }
}
