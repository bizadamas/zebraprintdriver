package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Form;

import org.junit.Test;

public class FormTest {

    @Test
    public void test() {
	Form command = new Form();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("FORM");

	assertCommand(output, command);
    }

}
