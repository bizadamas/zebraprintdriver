package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Journal;

import org.junit.Test;

public class JournalTest {

    @Test
    public void test() {
	Journal command = new Journal();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("JOURNAL");

	assertCommand(output, command);
    }

}
