package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.Size;
import it.stefanobertini.zebra.cpcl.labelmode.ExpandedGraphics;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class ExpandedGraphicsTest {

    @Test
    public void test1() {
	ExpandedGraphics command = new ExpandedGraphics(Orientation.horizontal, new Size(2, 16), new Position(90, 45),
	        "F0F0F0F0F0F0F0F00F0F0F0F0F0F0F0FF0F0F0F0F0F0F0F00F0F0F0F0F0F0F0F");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("EXPANDED-GRAPHICS 2 16 90 45 F0F0F0F0F0F0F0F00F0F0F0F0F0F0F0FF0F0F0F0F0F0F0F00F0F0F0F0F0F0F0F");

	assertCommand(output, command);
    }

    @Test
    public void test2() {
	int[] data = { 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0,
	        0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F };

	ExpandedGraphics command = new ExpandedGraphics(Orientation.vertical, new Size(2, 16), new Position(90, 45), data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("VEXPANDED-GRAPHICS 2 16 90 45 F0F0F0F0F0F0F0F00F0F0F0F0F0F0F0FF0F0F0F0F0F0F0F00F0F0F0F0F0F0F0F");

	assertCommand(output, command);
    }
}
