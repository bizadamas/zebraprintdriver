package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetBold;

import org.junit.Test;

public class SetBoldTest {

    @Test
    public void test() {
	SetBold command = new SetBold(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETBOLD 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetBold command = new SetBold();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETBOLD 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	SetBold command = new SetBold(-1);

	command.getCommandByteArray();
    }

}
