package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.EndPage;

import org.junit.Test;

public class EndPageTest {

    @Test
    public void test() {
	EndPage command = new EndPage();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 END-PAGE");

	assertCommand(output, command);
    }

}
