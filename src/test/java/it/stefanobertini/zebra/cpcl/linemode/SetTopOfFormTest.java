package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetTopOfForm;

import org.junit.Test;

public class SetTopOfFormTest {

    @Test
    public void test() {
	SetTopOfForm command = new SetTopOfForm(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U");
	output.printLn("SET-TOF 10");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetTopOfForm command = new SetTopOfForm();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U");
	output.printLn("SET-TOF 0");
	output.printLn("PRINT");
	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	SetTopOfForm command = new SetTopOfForm(-1);

	command.getCommandByteArray();
    }
}
