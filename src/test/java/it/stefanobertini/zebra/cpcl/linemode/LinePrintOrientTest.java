package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.LinePrintOrient;
import it.stefanobertini.zebra.enums.LpOrientation;

import org.junit.Test;

public class LinePrintOrientTest {

    @Test
    public void test() {
	LinePrintOrient command = new LinePrintOrient(LpOrientation.vertical);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 LP-ORIENT 270");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	LinePrintOrient command = new LinePrintOrient();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 LP-ORIENT 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	LinePrintOrient command = new LinePrintOrient("");

	command.getCommandByteArray();
    }
}
