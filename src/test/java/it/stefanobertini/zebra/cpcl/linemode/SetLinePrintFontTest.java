package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.cpcl.linemode.SetLinePrintFont;

import org.junit.Test;

public class SetLinePrintFontTest {

    @Test
    public void test() {
	SetLinePrintFont command = new SetLinePrintFont(new Font("4", 0), 1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETLP 4 0 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	SetLinePrintFont command = new SetLinePrintFont(new Font("4", 0), -1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	SetLinePrintFont command = new SetLinePrintFont(null, 1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("");

	assertCommand(output, command);
    }
}
