package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.Barcode;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;

import org.junit.Test;

public class BarcodeTest {

    @Test
    public void test() {

	Barcode command = new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, "HORIZ.");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 BARCODE 128 1 1 50 0 0 HORIZ.");

	assertCommand(output, command);
    }

}
