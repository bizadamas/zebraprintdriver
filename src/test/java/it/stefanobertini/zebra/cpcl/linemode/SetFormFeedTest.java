package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetFormFeed;

import org.junit.Test;

public class SetFormFeedTest {

    @Test
    public void test() {
	SetFormFeed command = new SetFormFeed(1, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U");
	output.printLn("SETFF 1 2");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetFormFeed command = new SetFormFeed();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U");
	output.printLn("SETFF 0 0");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	SetFormFeed command = new SetFormFeed(-1, 0);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	SetFormFeed command = new SetFormFeed(0, -1);

	command.getCommandByteArray();
    }
}
