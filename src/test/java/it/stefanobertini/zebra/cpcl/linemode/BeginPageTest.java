package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.BeginPage;

import org.junit.Test;

public class BeginPageTest {

    @Test
    public void test() {
	BeginPage command = new BeginPage();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 BEGIN-PAGE");

	assertCommand(output, command);
    }

}
