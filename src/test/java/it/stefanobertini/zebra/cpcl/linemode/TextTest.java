package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.Text;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TextTest {

    @Test
    public void test1() {
	Text command = new Text();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.print("");

	assertCommand(output, command);
    }

    @Test
    public void test2() {
	Text command = new Text("WITH NEWLINE");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("WITH NEWLINE");

	assertCommand(output, command);
    }

    @Test
    public void test3() {
	Text command = new Text("WITH NEWLINE", true);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("WITH NEWLINE");

	assertCommand(output, command);
    }

    @Test
    public void test4() {
	Text command = new Text("WITHOUT NEWLINE", false);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.print("WITHOUT NEWLINE");

	assertCommand(output, command);
    }

    @Test
    public void test5() {
	List<String> data = new ArrayList<String>();
	data.add("1");
	data.add("2");
	data.add("3");
	Text command = new Text(data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("1");
	output.printLn("2");
	output.printLn("3");

	assertCommand(output, command);
    }

    @Test
    public void test6() {
	List<String> data = new ArrayList<String>();
	data.add("1");
	data.add("2");
	data.add("3");
	Text command = new Text(data, true);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("1");
	output.printLn("2");
	output.printLn("3");

	assertCommand(output, command);
    }

    @Test
    public void test7() {
	List<String> data = new ArrayList<String>();
	data.add("1");
	data.add("2");
	data.add("3");
	Text command = new Text(data, false);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.print("123");

	assertCommand(output, command);
    }

}
