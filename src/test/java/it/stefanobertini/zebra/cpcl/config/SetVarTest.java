package it.stefanobertini.zebra.cpcl.config;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.config.SetVar;
import it.stefanobertini.zebra.enums.ConfigurationKey;

import org.junit.Test;

public class SetVarTest {

    @Test
    public void test() {
	SetVar command = new SetVar(ConfigurationKey.commBaud, "9600");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 setvar \"comm.baud\" \"9600\"");

	assertCommand(output, command);
    }

    @Test(expected = RuntimeException.class)
    public void testBadKey() {
	new SetVar(ConfigurationKey.deviceReset, "");
    }

    @Test(expected = RuntimeException.class)
    public void testNullKey() {
	new SetVar(null, "");
    }

    @Test
    public void testToSting() {
	new SetVar(ConfigurationKey.commBaud, "9600").toString();
    }
}
