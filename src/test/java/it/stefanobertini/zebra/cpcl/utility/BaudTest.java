package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.Baud;
import it.stefanobertini.zebra.enums.BaudRate;

import org.junit.Test;

public class BaudTest {

    @Test
    public void testDefault() {
	Baud command = new Baud();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("BAUD 115200");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testString() {
	Baud command = new Baud("1234");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("BAUD 1234");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testEnum() {
	Baud command = new Baud(BaudRate.bauds_19200);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("BAUD 19200");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Baud command = new Baud("");

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	Baud command = new Baud();
	command.setBaudRate(BaudRate.bauds_115200);
	command.setBaudRate("");
	command.getBaudRate();
	command.toString();
    }
}
