package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.GetDate;

import org.junit.Test;

public class GetDateTest {

    @Test
    public void test() {
	GetDate command = new GetDate();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("GET-DATE");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	GetDate command = new GetDate();
	command.toString();
    }
}
