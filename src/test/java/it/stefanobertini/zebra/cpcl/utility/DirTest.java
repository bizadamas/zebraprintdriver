package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.Dir;

import org.junit.Test;

public class DirTest {

    @Test
    public void test() {
	Dir command = new Dir();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("DIR");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	Dir command = new Dir();
	command.toString();
    }
}
