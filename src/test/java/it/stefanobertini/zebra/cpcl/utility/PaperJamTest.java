package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.PaperJam;
import it.stefanobertini.zebra.enums.PaperJamMethod;

import org.junit.Test;

public class PaperJamTest {

    @Test
    public void testDefault() {
	PaperJam command = new PaperJam();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM PRESENTATION 0");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test1() {
	PaperJam command = new PaperJam(PaperJamMethod.bar, 2, "MESSAGE");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM BAR 2 ALERT \"MESSAGE\"");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test2() {
	PaperJam command = new PaperJam(PaperJamMethod.bar, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM BAR 2");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test3() {
	PaperJam command = new PaperJam(PaperJamMethod.gap, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM GAP 2");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test4() {
	PaperJam command = new PaperJam(PaperJamMethod.presentation, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM PRESENTATION 2");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test5() {
	PaperJam command = new PaperJam(PaperJamMethod.presentation, 2, null);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("PAPER-JAM PRESENTATION 2");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	PaperJam command = new PaperJam();
	command.setAlertMessage("");
	command.setBarDistance(0);
	command.setPaperJamMethod(PaperJamMethod.bar);
	command.getAlertMessage();
	command.getBarDistance();
	command.getPaperJamMethod();
	command.toString();
    }
}
