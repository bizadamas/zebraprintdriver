package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.SetLineTerminatorCharacter;
import it.stefanobertini.zebra.enums.LineTerminatorCharacter;

import org.junit.Test;

public class SetLineTerminatorCharacterTest {

    @Test
    public void test1() {
	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("LT CR-LF");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter(LineTerminatorCharacter.carriageReturn);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("LT CR");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test3() {
	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter(LineTerminatorCharacter.lineFeed);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("LT LF");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test4() {
	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter(LineTerminatorCharacter.carriageReturnAndLineFeed);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("LT CR-LF");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test5() {
	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter(LineTerminatorCharacter.carriageReturnAndAnyThenLineFeed);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("LT CR-X-LF");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	SetLineTerminatorCharacter command = new SetLineTerminatorCharacter();
	command.setLineTerminatorCharacter(LineTerminatorCharacter.carriageReturn);
	command.getLineTerminatorCharacter();
	command.toString();
    }
}
