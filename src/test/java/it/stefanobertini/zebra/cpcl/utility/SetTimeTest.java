package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.SetTime;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class SetTimeTest {

    @Test
    public void test() {
	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
	Date time = new Date();

	SetTime command = new SetTime(time);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("SET-TIME " + format.format(time));
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	SetTime command = new SetTime();
	command.setTime(null);
	command.getTime();
	command.setTime(new Date());
	command.getTime();
	command.toString();
    }
}
