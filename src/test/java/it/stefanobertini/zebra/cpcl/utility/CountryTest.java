package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.Country;
import it.stefanobertini.zebra.enums.CountryCode;

import org.junit.Test;

public class CountryTest {

    @Test
    public void testString() {
	Country command = new Country("USA");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("COUNTRY USA");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testEnum() {
	Country command = new Country(CountryCode.usa);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("COUNTRY USA");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	Country command = new Country();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("COUNTRY USA");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	Country command = new Country();
	command.setCountryCode(CountryCode.big5);
	command.setCountryCode("");
	command.getCountryCode();
	command.toString();
    }
}
