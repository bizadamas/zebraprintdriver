package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.GetTime;

import org.junit.Test;

public class GetTimeTest {

    @Test
    public void test() {
	GetTime command = new GetTime();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("GET-TIME");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	GetTime command = new GetTime();
	command.toString();
    }
}
