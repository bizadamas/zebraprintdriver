package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.OnLowBattery;

import org.junit.Test;

public class OnLowBatteryTest {

    @Test(expected = ValidationException.class)
    public void test1() {
	OnLowBattery command = new OnLowBattery();

	command.getCommandByteArray();
    }

    @Test
    public void test2() {
	OnLowBattery command = new OnLowBattery(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("OLB ALARM 1");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test3() {
	OnLowBattery command = new OnLowBattery("MESSAGE");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("OLB ALERT \"MESSAGE\"");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test4() {
	OnLowBattery command = new OnLowBattery("MESSAGE", 1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("OLB ALERT \"MESSAGE\" ALARM 1");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test5() {
	OnLowBattery command = new OnLowBattery(null, 1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("OLB ALARM 1");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	OnLowBattery command = new OnLowBattery();
	command.setAlertMessage("");
	command.setAlarmDuration(0);
	command.getAlertMessage();
	command.getAlarmDuration();
	command.toString();
    }
}
