package it.stefanobertini.zebra;

import static org.junit.Assert.assertArrayEquals;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.FormatUtils;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Abort;
import it.stefanobertini.zebra.cpcl.labelmode.End;

import org.junit.Test;

public class PrintJobTest {

    @Test
    public void test() {

	PrintJob<LabelModeCommandInterface> printJob = new PrintJob<LabelModeCommandInterface>();

	printJob.add(new Abort());
	printJob.add(new End());

	byte[] expected;

	expected = "ABORT\r\nEND\r\n".getBytes();

	assertArrayEquals(expected, printJob.getCommandByteArray());

    }

    @Test
    public void testChain() {

	PrintJob<LabelModeCommandInterface> printJob = new PrintJob<LabelModeCommandInterface>();

	printJob = printJob.add(new Abort()).add(new End());

	byte[] expected;

	expected = "ABORT\r\nEND\r\n".getBytes();

	assertArrayEquals(expected, printJob.getCommandByteArray());

    }

    @Test(expected = RuntimeException.class)
    public void testException() {
	class FakeClass implements CommandInterface {

	    public String getCommand() {
		return null;
	    }

	    public byte[] getCommandByteArray() {
		return null;
	    }

	    public void validate() throws ValidationException {
	    }

	}

	PrintJob<CommandInterface> printJob = new PrintJob<CommandInterface>();

	printJob.add(new FakeClass());
	printJob.getCommandByteArray();
    }

    @Test
    public void test2() {
	new FormatUtils();
    }

    @Test
    public void testToString() {
	new PrintJob<CommandInterface>().toString();
    }
}
