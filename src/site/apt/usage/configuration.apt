Configuration commands

	Configuration commands (<<SetVar>>, <<GetVar>> and <<Do>>) are used to read and write the printer's configuration and to issue some commands to the printer.
	
	The <<configuration keys>> that can be used with those commands are defined in an enumeration(<<ConfigurationKey>>) to minimize the possibility of errors.
	
	Configuration Commands are implemented in the <it.bertasoft.zebra.cpcl.config> package.

+------
NetworkPrintDriver driver = new NetworkPrintDriver...;
String originalValue;
String answer;

// If you want to see what is happening
driver.setDebugStream(System.out); 

// If you want to specify a custom timeout for answers
driver.setDefaultAnswerWaitMillis(1000);

originalValue = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
System.out.println("Original value: " + originalValue);

driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "off"));
answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
System.out.println("After setVar(off): " + answer);

driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "on"));
answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
System.out.println("After setVar(on): " + answer);

driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, originalValue));
answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
System.out.println("After setVar(originalValue): " + answer);
+------