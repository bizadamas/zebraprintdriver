#Zebra CPLC Network Print Driver for Java

This library implements a Java Driver to print to a Zebra Printer.

It can generate the CPLC code for every Zebra command described in the CPCL user guide.

The commands are divided in **four** main categories:

* Label Mode commands
* Line Mode commands
* Configuration commands
* Utility commands

Label Mode and Line Mode commands can be joined together using a *PrintJob*.

Commands and PrintJobs can be sent to a *Network Printer* using a *NetworkPrintDriver* or can be sent via serial port.

You can find a set of usage samples [here](https://bitbucket.org/sbertini/zebraprintdriversamples).


###License and donations
If you find this library useful, please, consider donating something.

##Label Mode Commands

Label Mode Commands are implemented in the *it.bertasoft.zebra.cpcl.labelmode* package.

To join Label Mode commands in a PrintJob, you can:

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(30, 40), "Hello World"));

	job.add(new Print());

	byte[] jobData ;
	jobData = job.getCommandLineByteArray();
	
	// Do something with jobData...

##Line Mode Commands

Line Mode Commands are implemented in the *it.bertasoft.zebra.cpcl.linemode* package.

To join Label Mode commands in a PrintJob, you can:

	PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
	job.add(new BeginPage());
	job.add(new Journal());

	job.add(new SetLinePrintFont(new Font("4", 0), 47));
	job.add(new Text("YOURCO RETAIL STORES"));
	job.add(new Text(""));

	job.add(new SetLinePrintFont(new Font("7", 0), 24));

	List<String> data = new ArrayList<String>();
	data.add("14:40 PM Thursday, 06/04/20");
	data.add("Quantity  Item          Unit     Total");
	data.add("1         Babelfish     $4.20    $4.20");
	data.add("          Tax:          5%       $0.21");
	job.add(new Text(data));
	job.add(new Text(""));

	job.add(new SetSpacing(5));
	job.add(new Text("Total:", false));

	job.add(new SetSpacing(0));
	job.add(new Text("$4.41", true));

	job.add(new Text("Thank you for shopping at YOURCO"));

Please, remember that Line Mode must be enabled on the printer by issuing a **setvar** command:

    driver.executeNoWait(new SetVar(ConfigurationKey.deviceLanguages, "line_print"));
  
##Sending a job to a printer

To send a job to a network printer, you can:

	NetworkPrintDriver driver = new NetworkPrintDriver(address, port);

	try {
		driver.open();
		driver.execute(job);
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		driver.close();
	}
	    
    
##Configuration commands
Configuration commands (*setvar*, *getvar* and *do*) are used to read and write the printer's configuration and to issue some commands to the printer.

The *configuration keys* that can be used with those commands are defined in an enumeration(*ConfigurationKey*) to minimize the possibility of errors.

Configuration Commands are implemented in the *it.bertasoft.zebra.cpcl.config* package.

	NetworkPrintDriver driver = new NetworkPrintDriver...;
	String originalValue;
	String answer;

	// If you want to see what is happening
	driver.setDebugStream(System.out); 
	driver.setDefaultAnswerWaitMillis(1000);

	originalValue = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
	System.out.println("Original value: " + originalValue);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "off"));
	answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(off): " + answer);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "on"));
	answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(on): " + answer);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, originalValue));
	answer = driver.executeAndWait(new GetVar(ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(originalValue): " + answer);
	
##Utility commands

Utility commands are used to manage the flash file system, obtain information about firmware and printer applications, configure the printer and to set several operating parameters.

Utility Commands are implemented in the *it.bertasoft.zebra.cpcl.utility* package.

They can be used as follows:

	NetworkPrintDriver driver = new NetworkPrintDriver...;
	String answer;
	String filename = "test.txt";
	List<String> data = new ArrayList<String>();

	// If you want to see what is happening
	driver.setDebugStream(System.out);

	data.add("Line1");
	data.add("Line2");
	data.add("Line3");

	answer = driver.executeAndWait(new DefineFile(filename, data, DefineFileTerminator.end));

	System.out.println("Answer: " + answer);

